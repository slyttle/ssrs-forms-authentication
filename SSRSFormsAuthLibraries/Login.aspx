<%@ Page language="c#" Codebehind="Login.aspx.cs" AutoEventWireup="false" Inherits="Microsoft.Samples.ReportingServices.CustomSecurity.Login, Microsoft.Samples.ReportingServices.CustomSecurity" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
   <HEAD>
      <title>eDofE SQL Server Reporting Services</title>
   </HEAD>
   <body>
      <form id="Form1" method="post" runat="server">
         <asp:Label id="LblTitle" style="Z-INDEX: 101; LEFT: 80px; POSITION: absolute; TOP: 56px" runat="server" Width="416px" Height="32px" Font-Size="Medium" Font-Names="Verdana">SQL Server Reporting Services</asp:Label>
         <asp:Label id="LblInstruction" style="Z-INDEX: 102; LEFT: 80px; POSITION: absolute; TOP: 86px" runat="server" Width="416px" Height="32px" Font-Size="Small" Font-Names="Verdana">
             It looks like your session has timed out. If you know your Reporting Services username and password, enter it below. Otherwise, please return to eDofE to login again.
         </asp:Label>
         <asp:Label id="LblUser" style="Z-INDEX: 103; LEFT: 116px; POSITION: absolute; TOP: 152px" runat="server" Width="96px" Font-Size="Small" Font-Names="Verdana" Font-Bold="True">Username:</asp:Label>
         <asp:TextBox id="TxtPwd" style="Z-INDEX: 104; LEFT: 236px; POSITION: absolute; TOP: 184px" runat="server" tabIndex="2" Width="160px" TextMode="Password"></asp:TextBox>
         <asp:Label id="LblPwd" style="Z-INDEX: 105; LEFT: 116px; POSITION: absolute; TOP: 192px" runat="server" Width="96px" Font-Size="Small" Font-Names="Verdana" Font-Bold="True">Password:</asp:Label>&nbsp;
         <asp:TextBox id="TxtUser" style="Z-INDEX: 106; LEFT: 236px; POSITION: absolute; TOP: 152px" runat="server" tabIndex="1" Width="160px"></asp:TextBox>
         <asp:Button id="BtnRegister" style="Z-INDEX: 107; LEFT: 172px; POSITION: absolute; TOP: 224px" runat="server" Width="104px" Text="Register User" tabIndex="4"></asp:Button>
         <asp:Button id="BtnLogon" style="Z-INDEX: 108; LEFT: 292px; POSITION: absolute; TOP: 224px" runat="server" Width="104px" Text="Logon" tabIndex="3"></asp:Button>
         <asp:Label id="lblMessage" style="Z-INDEX: 109; LEFT: 80px; POSITION: absolute; TOP: 262px" runat="server" Width="416px" Font-Size="Small" Font-Names="Verdana"></asp:Label>
      </form>
   </body>
</HTML>
