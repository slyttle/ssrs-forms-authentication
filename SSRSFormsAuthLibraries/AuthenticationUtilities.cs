#region Copyright � 2004 Microsoft Corporation. All rights reserved.
/*============================================================================
   File:      AuthenticationStore.cs

  Summary:  Demonstrates how to create and maintain a user store for
            a security extension. 
--------------------------------------------------------------------
  This file is part of Microsoft SQL Server Code Samples.
    
 This source code is intended only as a supplement to Microsoft
 Development Tools and/or on-line documentation. See these other
 materials for detailed information regarding Microsoft code 
 samples.

 THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
 ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 PARTICULAR PURPOSE.
===========================================================================*/
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Xml;
using System.Web;
using System.Web.Security;
using System.Web.Configuration;

namespace Microsoft.Samples.ReportingServices.CustomSecurity
{
    internal sealed class AuthenticationUtilities
    {
        // The path of any item in the report server database 
        // has a maximum character length of 260
        private const int MaxItemPathLength = 260;

        // Method used to create a cryptographic random number that is used to
        // salt the user's password for added security
        internal static string CreateToken(int size)
        {
            const string charSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~-_";
            System.Text.StringBuilder token = new System.Text.StringBuilder(size);
            
            // Generate a cryptographic random number using the cryptographic service provider
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[size];
            rng.GetBytes(buff);

            int index = 0;
            foreach (byte b in buff)
                token.Append(charSet[(index += b) % charSet.Length]);

            return token.ToString();
        }

        // Returns a hash of the combined password and salt value
        internal static string HashPassword(string pwd, string usr)
        {
            // Create salt from username
            string salt = FormsAuthentication.HashPasswordForStoringInConfigFile(usr.ToUpper(), "SHA1").Substring(0, 12);
            // Concat the raw password and salt value
            return FormsAuthentication.HashPasswordForStoringInConfigFile(string.Concat(salt, pwd), "SHA1");
        }

        // Stores the account details in a SQL table named UserAccounts
        internal static void StoreAccountDetails(string Username, string Password)
        {
            // See "How To Use DPAPI (Machine Store) from ASP.NET" for 
            // information about securely storing connection strings.
            SqlConnection conn = new SqlConnection(GetRSAuthConnStr());

            SqlCommand cmd = new SqlCommand("RefreshPassword", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@AppID", GetApplicationID()));
            cmd.Parameters.Add(new SqlParameter("@Username", Username));
            cmd.Parameters.Add(new SqlParameter("@Password", HashPassword(Password, Username)));

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // Code to check for primary key violation (duplicate account 
                // name) or other database errors omitted for clarity
                throw new Exception("An error occurred while attempting to add the account. " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        // Stores the account details in a SQL table named UserAccounts
        internal static void StoreAuthToken(string Username, string AuthToken)
        {
            // See "How To Use DPAPI (Machine Store) from ASP.NET" for 
            // information about securely storing connection strings.
            SqlConnection conn = new SqlConnection(GetRSAuthConnStr());

            SqlCommand cmd = new SqlCommand("RefreshAuthToken", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@AppID", GetApplicationID())); 
            cmd.Parameters.Add(new SqlParameter("@Username", Username));
            cmd.Parameters.Add(new SqlParameter("@Token", AuthToken));

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // Code to check for primary key violation (duplicate account 
                // name) or other database errors omitted for clarity
                throw new Exception("An error occurred while attempting to add the account. " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        // Method that indicates whether the supplied AppID and AppSecret are valid
        internal static bool VerifyApplication(string AppID, string AppSecret)
        {
            bool passwordMatch = false;
            SqlConnection conn = new SqlConnection(GetRSAuthConnStr());

            SqlCommand cmd = new SqlCommand("GetApplication", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@AppID", AppID));
            cmd.Parameters.Add(new SqlParameter("@AppSecret", AppSecret));

            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                // If a row exists for the user, then the user is valid.
                if (reader.Read())
                    passwordMatch = true;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while attempting to verify the application. " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return passwordMatch;
        }

        // Method that indicates whether the supplied username and password are valid
        internal static bool VerifyPassword(string Username, string Password)
        {
            bool passwordMatch = false;
            // Get the salt and pwd from the database based on the user name.
            // See "How To: Use DPAPI (Machine Store) from ASP.NET," "How To:
            // Use DPAPI (User Store) from Enterprise Services," and "How To:
            // Create a DPAPI Library" on MSDN for more information about 
            // how to use DPAPI to securely store connection strings.
            SqlConnection conn = new SqlConnection(GetRSAuthConnStr());

            SqlCommand cmd = new SqlCommand("GetUser", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@AppID", GetApplicationID()));
            cmd.Parameters.Add(new SqlParameter("@Username", Username));
            cmd.Parameters.Add(new SqlParameter("@Password", HashPassword(Password, Username)));

            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                // If a row exists for the user, then the user is valid.
                if (reader.Read())
                    passwordMatch = true;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while attempting to verify the user. " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return passwordMatch;
        }

        // Method that indicates whether the supplied AuthToken is valid
        internal static string VerifyAuthToken(string AuthToken)
        {
            string Username = "";
            SqlConnection conn = new SqlConnection(GetRSAuthConnStr());

            SqlCommand cmd = new SqlCommand("GetUser", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@AppID", GetApplicationID()));
            cmd.Parameters.Add(new SqlParameter("@Token", AuthToken));

            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                
                // Grab the username if the token exists.
                if(reader.Read())
                    Username = reader.GetString(0);
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while attempting to verify the user. " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return Username;
        }

        // Called by the IsValidPrincipalName method which is used by the report server when the report server sets security on an item. This method validates 
        // that the user name is valid for Windows.The principal name needs to be a user, group, or builtin account name.
        internal static bool VerifyUser(string Username)
        {
            //If username is the "default identity" return true.            
            if (0 == String.Compare(Username, GetDefaultIdentity(), true, System.Globalization.CultureInfo.CurrentCulture))
                return true;

            bool isValid = false;
            SqlConnection conn = new SqlConnection(GetRSAuthConnStr());

            SqlCommand cmd = new SqlCommand("GetUser", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@AppID", GetApplicationID()));
            cmd.Parameters.Add(new SqlParameter("@Username", Username));

            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                // If a row exists for the user, then the user is valid.
                if (reader.Read())
                    isValid = true;
            }
            catch (Exception ex)
            {
                throw new Exception("Exception verifying user. " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return isValid;
        }

        // Method to verify that the user name does not contain any illegal characters. If My Reports is enabled, illegal characters
        // will invalidate the paths created in the \Users folder. Usernames should not contain the characters captured by this method.
        internal static bool ValidateUserName(string input)
        {
            Regex r = new Regex(@"\A(\..*)?[^\. ](.*[^ ])?\z(?<=\A[^/;\?:@&=\+\$,\\\*<>\|""]{0," +
               MaxItemPathLength.ToString() + @"}\z)",
               RegexOptions.Compiled | RegexOptions.ExplicitCapture);
            bool isValid = r.IsMatch(input);
            return isValid;
        }

        //Method to get the RSAuthentication DB connection string from the current web.config file.
        private static string GetRSAuthConnStr()
        {
            if (!string.IsNullOrEmpty(WebConfigurationManager.AppSettings["RSAuthenticationConnectionString"]))
                return WebConfigurationManager.AppSettings["RSAuthenticationConnectionString"];

            throw new Exception("The AppSetting 'RSAuthConnStr' is missing from the current web.config file.");
        }

        // Method to get the path to the RSReportServer.config file. This file can be used to retrieve the server URL for the Web service proxy.
        private static string GetConfigurationFilePath()
        {
            if(!string.IsNullOrEmpty(WebConfigurationManager.AppSettings["ConfigFilePath"]))
                return WebConfigurationManager.AppSettings["ConfigFilePath"];

            throw new Exception("The AppSetting 'ConfigFilePath' is missing from the current web.config file.");
        }

        // Method to get an item from the configuration file at the given Xpath.
        internal static string GetConfigItem(string Xpath)
        {
            string configPath = GetConfigurationFilePath();

            try
            {
                // Load the file into an XmlDocument
                XmlDocument config = new XmlDocument();
                config.Load(configPath);

                XmlNode node = config.SelectSingleNode(Xpath);

                if (node != null)
                    return node.InnerText;
                else
                    throw new NullReferenceException("The Report Server configuration file does not contain an element at '" + Xpath + "'.");

            }
            catch (NullReferenceException exp)
            {
                throw exp;
            }
            catch (Exception exp)
            {
                throw new Exception("Cound not read the Report Server configuration file. Plese ensure your web.config files contain the correct 'ConfigFilePath' AppSetting.");
            }
        }

        // Method to get the URL of the Reporting Services Web service on the local report server.
        internal static string GetReportServerUrl()
        {
            return GetConfigItem("/Configuration/UI/ReportServerUrl");
        }

        // Method to get the DIR of the Reporting Services Web service on the local report server.
        internal static string GetReportServerDir()
        {
            if (!string.IsNullOrEmpty(WebConfigurationManager.AppSettings["ReportServerDir"]))
                return WebConfigurationManager.AppSettings["ReportServerDir"];

            throw new Exception("The AppSetting 'ReportServerDir' is missing from the current web.config file.");
        }

        // Method to get the DIR of the Reporting Services Web service on the local report server.
        internal static string GetReportManagerDir()
        {
            if (!string.IsNullOrEmpty(WebConfigurationManager.AppSettings["ReportManagerDir"]))
                return WebConfigurationManager.AppSettings["ReportManagerDir"];

            throw new Exception("The AppSetting 'ReportManagerDir' is missing from the current web.config file.");
        }

        // Method to get the application ID for remote authentication.
        internal static string GetApplicationID()
        {
            if (!string.IsNullOrEmpty(WebConfigurationManager.AppSettings["ApplicationID"]))
                return WebConfigurationManager.AppSettings["ApplicationID"];

            throw new Exception("The AppSetting 'ApplicationID' is missing from the current web.config file.");
        }

        // Method to get the application ID for remote authentication.
        internal static string GetDefaultIdentity()
        {
            if (!string.IsNullOrEmpty(WebConfigurationManager.AppSettings["DefaultIdentity"]))
                return WebConfigurationManager.AppSettings["DefaultIdentity"];

            throw new Exception("The AppSetting 'DefaultIdentity' is missing from the current web.config file.");
        }
    }
}
