using System;
using System.Web.Configuration;

namespace Microsoft.Samples.ReportingServices.CustomSecurity
{
    public class SSO : System.Web.UI.Page
    {
        private void Page_Load(object sender, EventArgs e)
        {
            string preUrl = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, "") + "/";
            string ReportServerUrl = preUrl + AuthenticationUtilities.GetReportServerDir();
            string ReportManagerUrl = preUrl + AuthenticationUtilities.GetReportManagerDir();

            //Allow users to sign in with a single username if enabled.
            DoDebugMode(ReportServerUrl, ReportManagerUrl);

            //Redirect Auth Token user to login page.
            DoAuthenticateUser(ReportServerUrl, ReportManagerUrl);

            //Otherwise Authenticate the application and return the user AuthToken.
            DoAuthenticateApplication();

            //Don't use this page - if we come here, redirect to ReportServer immediatly.
            Response.Redirect(ReportServerUrl + "/Login.aspx" + Request.Url.Query, true);

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Load += new System.EventHandler(Page_Load);
        }
        #endregion

        private void DoDebugMode(string ReportServerUrl, string ReportManagerUrl)
        {
            if (!String.IsNullOrEmpty(WebConfigurationManager.AppSettings["DebugMode"]))
            {
                //If username provided forward to temporary authentication process.
                if (!string.IsNullOrEmpty(Request.QueryString["username"]))
                {
                    string AuthToken = AuthenticationUtilities.CreateToken(128);
                    AuthenticationUtilities.StoreAuthToken(Request.QueryString["username"], AuthToken);

                    Response.Redirect(ReportServerUrl + "/Login.aspx?ReturnUrl=" + Uri.EscapeDataString(ReportManagerUrl + "/Pages/Folder.aspx") + "&AuthToken=" + Uri.EscapeDataString(AuthToken), true);
                }
            }
        }

        private void DoAuthenticateUser(string ReportServerUrl, string ReportManagerUrl)
        {
            //If AuthToken provided forward to authentication process.
            if (!string.IsNullOrEmpty(Request.QueryString["AuthToken"]))
            {
                Response.Redirect(ReportServerUrl + "/Login.aspx?ReturnUrl=" + Uri.EscapeDataString(ReportManagerUrl + "/Pages/Folder.aspx") + "&AuthToken=" + Uri.EscapeDataString(Request.QueryString["AuthToken"]), true);
            }
        }

        private void DoAuthenticateApplication()
        {
            if (!String.IsNullOrEmpty(Request.Params["Username"]))
            {
                string response = "";
                try
                {
                    if (string.IsNullOrEmpty(Request.Params["AppID"]))
                        throw new Exception("Missing parameter: 'AppID'");

                    if (string.IsNullOrEmpty(Request.Params["AppSecret"]))
                        throw new Exception("Missing parameter: 'AppSecret'");

                    if (!AuthenticationUtilities.VerifyApplication(Request.Params["AppID"], Request.Params["AppSecret"]))
                        throw new Exception("Unable to verify application. Invalid AppSecret / ID combination.");

                    //Create new Auth token and record it against the given username.
                    response = AuthenticationUtilities.CreateToken(128);
                    AuthenticationUtilities.StoreAuthToken(Request.Params["Username"], response);
                }
                catch (Exception exp)
                {
                    response = "Error: " + exp.Message;
                }

                //Do Response
                Response.Clear();
                Response.ContentType = "text/plain";

                if (!string.IsNullOrEmpty(Request.Params["Format"]))
                { 
                    switch (Request.Params["Format"].ToUpper())
                    {
                        case "JSON":
                            Response.ContentType = "application/json";
                            if(response.StartsWith("Error:"))
                                response = string.Concat("{ \"Error\": \"", response.Substring(7), "\" }");
                            else
                                response = string.Concat("{ \"AuthToken\": \"", response, "\" }");

                            if (!string.IsNullOrEmpty(Request.Params["callback"]))
                            {
                                Response.ContentType = "application/javascript";
                                response = string.Concat(Request.Params["callback"], "(", response, ");");
                            }
                            break;
                        case "XML":
                            Response.ContentType = "text/xml";
                            if (response.StartsWith("Error:"))
                                response = string.Concat("<Error>", response.Substring(7), "</Error>");
                            else
                                response = string.Concat("<AuthToken>", response, "</AuthToken>");
                            break;
                    }
                }

                Response.Write(response);
                Response.End();
            }
        }

    }
}
