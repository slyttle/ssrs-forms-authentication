USE master
GO
-- create a database for the security information
IF EXISTS (SELECT * FROM   master..sysdatabases WHERE  name =
'UserAccounts')
  DROP DATABASE UserAccounts
GO
CREATE DATABASE UserAccounts
GO
USE UserAccounts
GO
CREATE TABLE [Users] (
  [UserName] [varchar] (255) NOT NULL ,
  [PasswordHash] [varchar] (40) NOT NULL ,
  [salt] [varchar] (10) NOT NULL,
  CONSTRAINT [PK_Users] PRIMARY KEY  CLUSTERED
  (
    [UserName]
  )  ON [PRIMARY] 
) ON [PRIMARY]
GO
-- create a stored procedure to register user details
CREATE PROCEDURE RegisterUser
@userName varchar(255),
@passwordHash varchar(40),
@salt varchar(10)
AS
INSERT INTO Users VALUES(@userName, @passwordHash, @salt)
GO
-- create a stored procedure to retrieve user details
CREATE PROCEDURE LookupUser
@userName varchar(255)
AS
SELECT PasswordHash, salt 
FROM Users
WHERE UserName = @userName
GO
-- Add a login for the local ASPNET account
-- In the following statements, replace LocalMachine with your
-- local machine name on Winows 2000 and Windows XP 
-- replace LocalMachine\ASPNET with NT AUTHORITY\NETWORK SERVICE 
-- on Windows 2003 (except when in IIS 5 compatibility mode)
exec sp_grantlogin [LocalMachine\ASPNET]
-- Add a database login for the UserAccounts database for the ASPNET account
exec sp_grantdbaccess [LocalMachine\ASPNET]
-- Grant execute permissions to the LookupUser and RegisterUser stored procs
grant execute on LookupUser to [LocalMachine\ASPNET]
grant execute on RegisterUser to [LocalMachine\ASPNET]