#region Copyright � 2004 Microsoft Corporation. All rights reserved.
/*============================================================================
  File:      AuthenticationExtension.cs

  Summary:  Demonstrates an implementation of an authentication 
            extension.
--------------------------------------------------------------------
  This file is part of Microsoft SQL Server Code Samples.
    
 This source code is intended only as a supplement to Microsoft
 Development Tools and/or on-line documentation. See these other
 materials for detailed information regarding Microsoft code 
 samples.

 THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
 ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 PARTICULAR PURPOSE.
===========================================================================*/
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using System.Security.Principal;
using System.Web;
using Microsoft.ReportingServices.Interfaces;

namespace Microsoft.Samples.ReportingServices.CustomSecurity
{
   public class AuthenticationExtension : IAuthenticationExtension
   {
      /// <summary>
      /// You must implement SetConfiguration as required by IExtension
      /// </summary>
      /// <param name="configuration">Configuration data as an XML
      /// string that is stored along with the Extension element in
      /// the configuration file.</param>
      public void SetConfiguration(String configuration)
      {
         // No configuration data is needed for this extension
      }

      /// <summary>
      /// You must implement LocalizedName as required by IExtension
      /// </summary>
      public string LocalizedName 
      {
         get
         {
            return null;
         }
      }

      /// <summary>
      /// Indicates whether a supplied username and password are valid.
      /// </summary>
      /// <param name="userName">The supplied username</param>
      /// <param name="password">The supplied password</param>
      /// <param name="authority">Optional. The specific authority to use to
      /// authenticate a user. For example, in Windows it would be a Windows 
      /// Domain</param>
      /// <returns>true when the username and password are valid</returns>
      public bool LogonUser(string userName, string password, string authority)
      {
         return AuthenticationUtilities.VerifyPassword(userName, password);
      }  
      
      /// <summary>
      /// Required by IAuthenticationExtension. The report server calls the 
      /// GetUserInfo methodfor each request to retrieve the current user 
      /// identity.
      /// </summary>
      /// <param name="userIdentity">represents the identity of the current 
      /// user. The value of IIdentity may appear in a user interface and 
      /// should be human readable</param>
      /// <param name="userId">represents a pointer to a unique user identity
      /// </param>
      public void GetUserInfo(out IIdentity userIdentity, out IntPtr userId)
      {
         // If the current user identity is not null,
         // set the userIdentity parameter to that of the current user 
         if (HttpContext.Current != null
               && HttpContext.Current.User != null)
         {
               userIdentity = HttpContext.Current.User.Identity;
         }
         else
               userIdentity = null;

         // initialize a pointer to the current user id to zero
         userId = IntPtr.Zero;
      }

      /// <summary>
      /// The IsValidPrincipalName method is called by the report server when 
      /// the report server sets security on an item. This method validates 
      /// that the user name is valid for Windows.The principal name needs to 
      /// be a user, group, or builtin account name.
      /// </summary>
      /// <param name="principalName">A user, group, or built-in account name
      /// </param>
      /// <returns>true when the principle name is valid</returns>
      public bool IsValidPrincipalName(string principalName)
      {
         return VerifyUser(principalName);
      }

      // 
      public static bool VerifyUser(string userName)
      { 
         bool isValid = false;
         SqlConnection conn = new SqlConnection(
            "Server=localhost;" + 
            "Integrated Security=SSPI;" +
            "database=UserAccounts");
         SqlCommand cmd = new SqlCommand("LookupUser", conn);
         cmd.CommandType = CommandType.StoredProcedure;

         SqlParameter sqlParam = cmd.Parameters.Add("@userName",
            SqlDbType.VarChar,
            255);
         sqlParam.Value = userName;
         try
         {
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            // If a row exists for the user, then the user is valid.
            if (reader.Read())
            {
               isValid = true;
            }
         }
         catch (Exception ex)
         {
            throw new Exception("Exception verifying user. " +
               ex.Message);
         }
         finally
         {
            conn.Close();
         }
         return isValid;
      }
   }
}
