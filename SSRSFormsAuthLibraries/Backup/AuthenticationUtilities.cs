#region Copyright � 2004 Microsoft Corporation. All rights reserved.
/*============================================================================
   File:      AuthenticationStore.cs

  Summary:  Demonstrates how to create and maintain a user store for
            a security extension. 
--------------------------------------------------------------------
  This file is part of Microsoft SQL Server Code Samples.
    
 This source code is intended only as a supplement to Microsoft
 Development Tools and/or on-line documentation. See these other
 materials for detailed information regarding Microsoft code 
 samples.

 THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
 ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 PARTICULAR PURPOSE.
===========================================================================*/
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web.Security;
using System.Management;
using System.Xml;

namespace Microsoft.Samples.ReportingServices.CustomSecurity
{
   internal sealed class AuthenticationUtilities
   {
      // The path of any item in the report server database 
      // has a maximum character length of 260
      private const int MaxItemPathLength = 260;

      // WMI constants
      const string            WmiNamespace = 
         @"\\localhost\root\Microsoft\SqlServer\ReportingServices\v8";
      const string            WmiRSClass = 
         @"MSReportServerReportManager_ConfigurationSetting";

      // Method used to create a cryptographic random number that is used to
      // salt the user's password for added security
      internal static string CreateSalt(int size)
      {
         // Generate a cryptographic random number using the cryptographic
         // service provider
         RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
         byte[] buff = new byte[size];
         rng.GetBytes(buff);
         // Return a Base64 string representation of the random number
         return Convert.ToBase64String(buff);
      }

      // Returns a hash of the combined password and salt value
      internal static string CreatePasswordHash(string pwd, string salt)
      {
         // Concat the raw password and salt value
         string saltAndPwd = String.Concat(pwd, salt);
         // Hash the salted password
         string hashedPwd = 
            FormsAuthentication.HashPasswordForStoringInConfigFile(
            saltAndPwd, "SHA1");
         return hashedPwd;
      }

      // Stores the account details in a SQL table named UserAccounts
      internal static void StoreAccountDetails(string userName,
         string passwordHash,
         string salt)
      {
         // See "How To Use DPAPI (Machine Store) from ASP.NET" for 
         // information about securely storing connection strings.
         SqlConnection conn = new SqlConnection( "Server=localhost;" +
            "Integrated Security=SSPI;" + "database=UserAccounts");

         SqlCommand cmd = new SqlCommand("RegisterUser", conn);
         cmd.CommandType = CommandType.StoredProcedure;
         SqlParameter sqlParam = null;

         sqlParam = cmd.Parameters.Add("@userName", SqlDbType.VarChar, 255);
         sqlParam.Value = userName;

         sqlParam = cmd.Parameters.Add(
            "@passwordHash ", SqlDbType.VarChar, 40);
         sqlParam.Value = passwordHash;

         sqlParam = cmd.Parameters.Add("@salt", SqlDbType.VarChar, 10);
         sqlParam.Value = salt;

         try
         {
            conn.Open();
            cmd.ExecuteNonQuery();
         }
         catch (Exception ex)
         {
            // Code to check for primary key violation (duplicate account 
            // name) or other database errors omitted for clarity
            throw new Exception("An error occurred while attempting to " + 
               "add the account. " + ex.Message);
         }
         finally
         {
            conn.Close();
         } 
      }

      // Method that indicates whether 
      // the supplied username and password are valid
      internal static bool VerifyPassword(string suppliedUserName,
         string suppliedPassword)
      { 
         bool passwordMatch = false;
         // Get the salt and pwd from the database based on the user name.
         // See "How To: Use DPAPI (Machine Store) from ASP.NET," "How To:
         // Use DPAPI (User Store) from Enterprise Services," and "How To:
         // Create a DPAPI Library" on MSDN for more information about 
         // how to use DPAPI to securely store connection strings.
         SqlConnection conn = new SqlConnection(
            "Server=localhost;" + 
            "Integrated Security=SSPI;" +
            "database=UserAccounts");
         SqlCommand cmd = new SqlCommand("LookupUser", conn);
         cmd.CommandType = CommandType.StoredProcedure;

         SqlParameter sqlParam = cmd.Parameters.Add("@userName",
            SqlDbType.VarChar,
            255);
         sqlParam.Value = suppliedUserName;
         try
         {
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read(); // Advance to the one and only row
            // Return output parameters from returned data stream
            string dbPasswordHash = reader.GetString(0);
            string salt = reader.GetString(1);
            // Now take the salt and the password entered by the user
            // and concatenate them together.
            string passwordAndSalt = String.Concat(suppliedPassword, salt);
            // Now hash them
            string hashedPasswordAndSalt =       
               FormsAuthentication.HashPasswordForStoringInConfigFile(
               passwordAndSalt,
               "SHA1");
            // Now verify them. Returns true if they are equal
            passwordMatch = hashedPasswordAndSalt.Equals(dbPasswordHash);
         }
         catch (Exception ex)
         {
            throw new Exception("An error occurred while attempting to " + 
               "verify the user. " + ex.Message);
         }
         finally
         {
            conn.Close();
         }
         return passwordMatch;
      }

      // Method to verify that the user name does not contain any
      // illegal characters. If My Reports is enabled, illegal characters
      // will invalidate the paths created in the \Users folder. Usernames
      // should not contain the characters captured by this method.
      internal static bool ValidateUserName(string input)
      {
         Regex r = new Regex(@"\A(\..*)?[^\. ](.*[^ ])?\z(?<=\A[^/;\?:@&=\+\$,\\\*<>\|""]{0," +
            MaxItemPathLength.ToString() + @"}\z)", 
            RegexOptions.Compiled | RegexOptions.ExplicitCapture);
         bool isValid = r.IsMatch(input);
         return isValid;
      }

      // Method to get the path to the RSWebApplication.config file.
      // This file can be used to retrieve the server URL for
      // the Web service proxy.
      private static string GetConfigurationFilePath()
      {
         string fullWmiClassName = WmiNamespace + ":" + WmiRSClass;
         ManagementClass serverClass;
         ManagementScope scope;
         scope = new ManagementScope(WmiNamespace);

         // Connect to the Reporting Services namespace.
         scope.Connect();
         // Create the server class.
         serverClass = new ManagementClass(fullWmiClassName);
         // Connect to the management object.
         try
         {
            serverClass.Get();
         }
         catch (Exception ex)
         {
            throw new Exception("Could not get WMI information. " 
               + ex.Message);
         }
            
         if (serverClass == null)
            throw new Exception(
               "No WMI class found. Cannot locate configuration file.");

         // Loop through the instances of the server class.
         ManagementObjectCollection instances = serverClass.GetInstances();

         foreach(ManagementObject instance in instances)
         {
            PropertyData pathProp = instance.Properties["PathName"];
            if(null == pathProp || null == pathProp.Value)
            {
               throw new Exception(
                  "WMI PathName property is null or does not exist");
            }
            else
               return pathProp.Value.ToString();
         }
         return null;
      }

      // Method to get the URL of the Reporting Services Web service on
      // the local report server.
      internal static string GetWebServiceUrl()
      {
         string url = "";
         string rsvroot = "";
         string path = GetConfigurationFilePath();
         if (path != null)
         {
            XmlDocument document = new XmlDocument();
            // Load the file into an XmlDocument
            document.Load(path);
            XmlNode root = document.FirstChild;

            foreach (XmlNode child in root.ChildNodes)
            {
               if (child.Name == "UI")
               {
                  foreach (XmlNode node in child.ChildNodes)
                  {
                     if (node.Name == "ReportServerUrl")
                     {
                        rsvroot = node.InnerText.ToString();
                     }
                  }
               }
            }
            url = rsvroot + @"/reportservice.asmx";
         }
         else
            throw new Exception("Cound not read the Report Manager " +
               "configuration file. Check to ensure your Reporting Services " +
               "WMI provider is working properly.");

         return url;
      }
   }
}
