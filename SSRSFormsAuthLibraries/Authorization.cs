#region Copyright � 2004 Microsoft Corporation. All rights reserved.
/*============================================================================
  File:     Authorization.cs

  Summary:  Demonstrates an implementation of an authorization 
            extension.
------------------------------------------------------------------------------
  This file is part of Microsoft SQL Server Code Samples.
    
 This source code is intended only as a supplement to Microsoft
 Development Tools and/or on-line documentation. See these other
 materials for detailed information regarding Microsoft code 
 samples.

 THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
 ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 PARTICULAR PURPOSE.
===========================================================================*/
#endregion

using System;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.ReportingServices.Interfaces;
using System.Xml;

namespace Microsoft.Samples.ReportingServices.CustomSecurity
{
    public class Authorization : IAuthorizationExtension
    {
        private static string m_adminUserName;
        private static string m_defaultIdentiy;
        static Authorization()
        {
            InitializeMaps();
        }

        /// <summary>
        /// Returns a security descriptor that is stored with an individual 
        /// item in the report server database. 
        /// </summary>
        /// <param name="acl">The access code list (ACL) created by the report 
        /// server for the item. It contains a collection of access code entry 
        /// (ACE) structures.</param>
        /// <param name="itemType">The type of item for which the security 
        /// descriptor is created.</param>
        /// <param name="stringSecDesc">Optional. A user-friendly description 
        /// of the security descriptor, used for debugging. This is not stored
        /// by the report server.</param>
        /// <returns>Should be implemented to return a serialized access code 
        /// list for the item.</returns>
        public byte[] CreateSecurityDescriptor(AceCollection acl, SecurityItemType itemType, out string stringSecDesc)
        {
            // Creates a memory stream and serializes the ACL for storage.
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream result = new MemoryStream();
            bf.Serialize(result, acl);
            stringSecDesc = null;
            return result.GetBuffer();
        }

        // Used to deserialize the ACL that is stored by the report server.
        private AceCollection DeserializeAcl(byte[] secDesc)
        {
            AceCollection acl = new AceCollection();
            if (secDesc != null)
            {
                BinaryFormatter bf = new BinaryFormatter();
                MemoryStream sdStream = new MemoryStream(secDesc);
                acl = (AceCollection)bf.Deserialize(sdStream);
            }
            return acl;
        }

        /// <summary>
        /// Indicates whether a given user is authorized to access the item 
        /// for a given catalog operation. (With default)
        /// </summary>
        /// <param name="userName">The name of the user as returned by the 
        /// GetUserInfo method.</param>
        /// <param name="userToken">Pointer to the user ID returned by 
        /// GetUserInfo.</param>
        /// <param name="secDesc">The security descriptor returned by 
        /// CreateSecurityDescriptor.</param>
        /// <param name="requiredOperation">The operation being requested by 
        /// the report server for a given user.</param>
        /// <returns>True if the user is authorized.</returns>
        public bool CheckAccess(string userName, IntPtr userToken, byte[] secDesc, CatalogOperation requiredOperation)
        {
            // If the user is the administrator, allow unrestricted access.
            // Because SQL Server defaults to case-insensitive, we have to
            // perform a case insensitive comparison. Ideally you would check
            // the SQL Server instance CaseSensitivity property before making
            // a case-insensitive comparison.
            if (0 == String.Compare(userName, m_adminUserName, true, CultureInfo.CurrentCulture))
                return true;
            
            //Assume the defaul response will be to deny access.
            bool defaultAccess = false;
            AceCollection acl = DeserializeAcl(secDesc);
            foreach (AceStruct ace in acl)
            {
                // First check to see if there is a default access control entry for the item.
                // Record Default access if it exist.
                if (0 == String.Compare(m_defaultIdentiy, ace.PrincipalName, true, CultureInfo.CurrentCulture))
                {
                    foreach (CatalogOperation aclOperation in ace.CatalogOperations)
                    {
                        if (aclOperation == requiredOperation)
                            defaultAccess = true;
                    }
                }
                // Next, check to see if the current user has an access control entry.
                // Return individual user access if it exists.
                else if (0 == String.Compare(userName, ace.PrincipalName, true, CultureInfo.CurrentCulture))
                {
                    // If an entry is found, 
                    // return true if the given required operation
                    // is contained in the ACE structure
                    foreach (CatalogOperation aclOperation in ace.CatalogOperations)
                    {
                        if (aclOperation == requiredOperation)
                            return true;
                    }

                    //Otherwise, return false immediately.
                    //User found but not granted permission
                    return false;
                }
            }

            //If user was not found - use the default access rule.
            return defaultAccess;
        }

        // Overload for array of catalog operations
        public bool CheckAccess(string userName, IntPtr userToken, byte[] secDesc, CatalogOperation[] requiredOperations)
        {
            foreach (CatalogOperation operation in requiredOperations)
            {
                if (!CheckAccess(userName, userToken, secDesc, operation))
                    return false;
            }
            return true;
        }

        // Overload for Report operations (With default)
        public bool CheckAccess(string userName, IntPtr userToken, byte[] secDesc, ReportOperation requiredOperation)
        {
            // If the user is the administrator, allow unrestricted access.
            if (0 == String.Compare(userName, m_adminUserName, true, CultureInfo.CurrentCulture))
                return true;

            bool defaultAccess = false;
            AceCollection acl = DeserializeAcl(secDesc);
            foreach (AceStruct ace in acl)
            {
                //Record Default access if it exist.
                if (0 == String.Compare(m_defaultIdentiy, ace.PrincipalName, true, CultureInfo.CurrentCulture))
                {
                    foreach (ReportOperation aclOperation in ace.ReportOperations)
                    {
                        if (aclOperation == requiredOperation)
                            defaultAccess = true;
                    }
                }
                //Return individual user access if it exists.
                else if (0 == String.Compare(userName, ace.PrincipalName, true, CultureInfo.CurrentCulture))
                {
                    foreach (ReportOperation aclOperation in ace.ReportOperations)
                    {
                        if (aclOperation == requiredOperation)
                            return true;
                    }

                    //User found but not granted permission
                    return false;
                }
            }

            return defaultAccess;
        }

        // Overload for Folder operations (With default)
        public bool CheckAccess(string userName, IntPtr userToken, byte[] secDesc, FolderOperation requiredOperation)
        {
            // If the user is the administrator, allow unrestricted access.
            if (0 == String.Compare(userName, m_adminUserName, true, CultureInfo.CurrentCulture))
                return true;

            bool defaultAccess = false;
            AceCollection acl = DeserializeAcl(secDesc);
            foreach (AceStruct ace in acl)
            {
                //Record Default access if it exist.
                if (0 == String.Compare(m_defaultIdentiy, ace.PrincipalName, true, CultureInfo.CurrentCulture))
                {
                    foreach (FolderOperation aclOperation in ace.FolderOperations)
                    {
                        if (aclOperation == requiredOperation)
                            defaultAccess = true;
                    }
                }
                //Return individual user access if it exists.
                else if (0 == String.Compare(userName, ace.PrincipalName, true, CultureInfo.CurrentCulture))
                {
                    foreach (FolderOperation aclOperation in ace.FolderOperations)
                    {
                        if (aclOperation == requiredOperation)
                            return true;
                    }

                    //User found but not granted permission
                    return false;
                }
            }

            return defaultAccess;
        }

        // Overload for an array of Folder operations
        public bool CheckAccess(string userName, IntPtr userToken, byte[] secDesc, FolderOperation[] requiredOperations)
        {
            foreach (FolderOperation operation in requiredOperations)
            {
                if (!CheckAccess(userName, userToken, secDesc, operation))
                    return false;
            }
            return true;
        }

        // Overload for Resource operations (With default)
        public bool CheckAccess(string userName, IntPtr userToken, byte[] secDesc, ResourceOperation requiredOperation)
        {
            // If the user is the administrator, allow unrestricted access.
            if (0 == String.Compare(userName, m_adminUserName, true, CultureInfo.CurrentCulture))
                return true;

            bool defaultAccess = false;
            AceCollection acl = DeserializeAcl(secDesc);
            foreach (AceStruct ace in acl)
            {
                //Record Default access if it exist.
                if (0 == String.Compare(m_defaultIdentiy, ace.PrincipalName, true, CultureInfo.CurrentCulture))
                {
                    foreach (ResourceOperation aclOperation in ace.ResourceOperations)
                    {
                        if (aclOperation == requiredOperation)
                            defaultAccess = true;
                    }
                }
                //Return individual user access if it exists.
                else if (0 == String.Compare(userName, ace.PrincipalName, true, CultureInfo.CurrentCulture))
                {
                    foreach (ResourceOperation aclOperation in ace.ResourceOperations)
                    {
                        if (aclOperation == requiredOperation)
                            return true;
                    }

                    //User found but not granted permission
                    return false;
                }
            }

            return defaultAccess;
        }

        // Overload for an array of Resource operations
        public bool CheckAccess(string userName, IntPtr userToken, byte[] secDesc, ResourceOperation[] requiredOperations)
        {
            foreach (ResourceOperation operation in requiredOperations)
            {
                if (!CheckAccess(userName, userToken, secDesc, operation))
                    return false;
            }
            return true;
        }

        // Overload for Datasource operations (With default)
        public bool CheckAccess(string userName, IntPtr userToken, byte[] secDesc, DatasourceOperation requiredOperation)
        {
            // If the user is the administrator, allow unrestricted access.
            if (0 == String.Compare(userName, m_adminUserName, true, CultureInfo.CurrentCulture))
                return true;

            bool defaultAccess = false;
            AceCollection acl = DeserializeAcl(secDesc);
            foreach (AceStruct ace in acl)
            {
                //Record Default access if it exist.
                if (0 == String.Compare(m_defaultIdentiy, ace.PrincipalName, true, CultureInfo.CurrentCulture))
                {
                    foreach (DatasourceOperation aclOperation in ace.DatasourceOperations)
                    {
                        if (aclOperation == requiredOperation)
                            defaultAccess = true;
                    }
                }
                //Return individual user access if it exists.
                else if (0 == String.Compare(userName, ace.PrincipalName, true, CultureInfo.CurrentCulture))
                {
                    foreach (DatasourceOperation aclOperation in ace.DatasourceOperations)
                    {
                        if (aclOperation == requiredOperation)
                            return true;
                    }

                    //User found but not granted permission
                    return false;
                }
            }

            return defaultAccess;
        }

        // Overload for Model operations (With default)
        public bool CheckAccess(string userName, IntPtr userToken, byte[] secDesc, ModelOperation requiredOperation)
        {
            // If the user is the administrator, allow unrestricted access.
            if (0 == String.Compare(userName, m_adminUserName, true, CultureInfo.CurrentCulture))
                return true;

            bool defaultAccess = false;
            AceCollection acl = DeserializeAcl(secDesc);
            foreach (AceStruct ace in acl)
            {
                //Record Default access if it exist.
                if (0 == String.Compare(m_defaultIdentiy, ace.PrincipalName, true, CultureInfo.CurrentCulture))
                {
                    foreach (ModelOperation aclOperation in ace.ModelOperations)
                    {
                        if (aclOperation == requiredOperation)
                            defaultAccess = true;
                    }
                }
                //Return individual user access if it exists.
                else if (0 == String.Compare(userName, ace.PrincipalName, true, CultureInfo.CurrentCulture))
                {
                    foreach (ModelOperation aclOperation in ace.ModelOperations)
                    {
                        if (aclOperation == requiredOperation)
                            return true;
                    }

                    //User found but not granted permission
                    return false;
                }
            }

            return defaultAccess;
        }

        // Overload for Model Item operations (With default)
        public bool CheckAccess(string userName, IntPtr userToken, byte[] secDesc, ModelItemOperation requiredOperation)
        {
            // If the user is the administrator, allow unrestricted access.
            if (0 == String.Compare(userName, m_adminUserName, true, CultureInfo.CurrentCulture))
                return true;

            bool defaultAccess = false;
            AceCollection acl = DeserializeAcl(secDesc);
            foreach (AceStruct ace in acl)
            {
                //Record Default access if it exist.
                if (0 == String.Compare(m_defaultIdentiy, ace.PrincipalName, true, CultureInfo.CurrentCulture))
                {
                    foreach (ModelItemOperation aclOperation in ace.ModelItemOperations)
                    {
                        if (aclOperation == requiredOperation)
                            defaultAccess = true;
                    }
                }
                //Return individual user access if it exists.
                else if (0 == String.Compare(userName, ace.PrincipalName, true, CultureInfo.CurrentCulture))
                {
                    foreach (ModelItemOperation aclOperation in ace.ModelItemOperations)
                    {
                        if (aclOperation == requiredOperation)
                            return true;
                    }

                    //User found but not granted permission
                    return false;
                }
            }

            return defaultAccess;
        }

        /// <summary>
        /// Returns the set of permissions a specific user has for a specific 
        /// item managed in the report server database. This provides underlying 
        /// support for the Web service method GetPermissions().
        /// </summary>
        /// <param name="userName">The name of the user as returned by the 
        /// GetUserInfo method.</param>
        /// <param name="userToken">Pointer to the user ID returned by 
        /// GetUserInfo.</param>
        /// <param name="itemType">The type of item for which the permissions 
        /// are returned.</param>
        /// <param name="secDesc">The security descriptor associated with the 
        /// item.</param>
        /// <returns></returns>
        public StringCollection GetPermissions(string userName, IntPtr userToken, SecurityItemType itemType, byte[] secDesc)
        {
            StringCollection permissions = new StringCollection();
            //If administrator, apply all permissions
            if (0 == String.Compare(userName, m_adminUserName, true, CultureInfo.CurrentCulture))
            {
                foreach (CatalogOperation oper in m_CatOperNames.Keys)
                {
                    if (!permissions.Contains((string)m_CatOperNames[oper]) && m_CatOperNames.ContainsKey(oper))
                        permissions.Add((string)m_CatOperNames[oper]);
                }
                foreach (ReportOperation oper in m_RptOperNames.Keys)
                {
                    if (!permissions.Contains((string)m_RptOperNames[oper]) && m_RptOperNames.ContainsKey(oper))
                        permissions.Add((string)m_RptOperNames[oper]);
                }
                foreach (FolderOperation oper in m_FldOperNames.Keys)
                {
                    if (!permissions.Contains((string)m_FldOperNames[oper]) && m_FldOperNames.ContainsKey(oper))
                        permissions.Add((string)m_FldOperNames[oper]);
                }
                foreach (ResourceOperation oper in m_ResOperNames.Keys)
                {
                    if (!permissions.Contains((string)m_ResOperNames[oper]) && m_ResOperNames.ContainsKey(oper))
                        permissions.Add((string)m_ResOperNames[oper]);
                }
                foreach (DatasourceOperation oper in m_DSOperNames.Keys)
                {
                    if (!permissions.Contains((string)m_DSOperNames[oper]) && m_DSOperNames.ContainsKey(oper))
                        permissions.Add((string)m_DSOperNames[oper]);
                }
                foreach (ModelOperation oper in m_MdlOperNames.Keys)
                {
                    if (!permissions.Contains((string)m_MdlOperNames[oper]) && m_MdlOperNames.ContainsKey(oper))
                        permissions.Add((string)m_MdlOperNames[oper]);
                }
                foreach (ModelItemOperation oper in m_MitOperNames.Keys)
                {
                    if (!permissions.Contains((string)m_MitOperNames[oper]) && m_MitOperNames.ContainsKey(oper))
                        permissions.Add((string)m_MitOperNames[oper]);
                }
            }
            else
            {
                //For all other users add selected permissions
                AceCollection acl = DeserializeAcl(secDesc);
                StringCollection defaultPermissions = new StringCollection();
                foreach (AceStruct ace in acl)
                {
                    //Record Default Permissions if they exist.
                    if (0 == String.Compare(m_defaultIdentiy, ace.PrincipalName, true, CultureInfo.CurrentCulture))
                    {
                        foreach (CatalogOperation aclOperation in ace.CatalogOperations)
                        {
                            if (!defaultPermissions.Contains((string)m_CatOperNames[aclOperation]) && m_CatOperNames.ContainsKey(aclOperation))
                                defaultPermissions.Add((string)m_CatOperNames[aclOperation]);
                        }
                        foreach (ReportOperation aclOperation in ace.ReportOperations)
                        {
                            if (!defaultPermissions.Contains((string)m_RptOperNames[aclOperation]) && m_RptOperNames.ContainsKey(aclOperation))
                                defaultPermissions.Add((string)m_RptOperNames[aclOperation]);
                        }
                        foreach (FolderOperation aclOperation in ace.FolderOperations)
                        {
                            if (!defaultPermissions.Contains((string)m_FldOperNames[aclOperation]) && m_FldOperNames.ContainsKey(aclOperation))
                                defaultPermissions.Add((string)m_FldOperNames[aclOperation]);
                        }
                        foreach (ResourceOperation aclOperation in ace.ResourceOperations)
                        {
                            if (!defaultPermissions.Contains((string)m_ResOperNames[aclOperation]) && m_ResOperNames.ContainsKey(aclOperation))
                                defaultPermissions.Add((string)m_ResOperNames[aclOperation]);
                        }
                        foreach (DatasourceOperation aclOperation in ace.DatasourceOperations)
                        {
                            if (!defaultPermissions.Contains((string)m_DSOperNames[aclOperation]) && m_DSOperNames.ContainsKey(aclOperation))
                                defaultPermissions.Add((string)m_DSOperNames[aclOperation]);
                        }
                        foreach (ModelOperation aclOperation in ace.ModelOperations)
                        {
                            if (!defaultPermissions.Contains((string)m_MdlOperNames[aclOperation]) && m_MdlOperNames.ContainsKey(aclOperation))
                                defaultPermissions.Add((string)m_MdlOperNames[aclOperation]);
                        }
                        foreach (ModelItemOperation aclOperation in ace.ModelItemOperations)
                        {
                            if (!defaultPermissions.Contains((string)m_MitOperNames[aclOperation]) && m_MitOperNames.ContainsKey(aclOperation))
                                defaultPermissions.Add((string)m_MitOperNames[aclOperation]);
                        }
                    }
                    //Apply individual user permissions if they exist.
                    else if (0 == String.Compare(userName, ace.PrincipalName, true, CultureInfo.CurrentCulture))
                    {
                        foreach (CatalogOperation aclOperation in ace.CatalogOperations)
                        {
                            if (!permissions.Contains((string)m_CatOperNames[aclOperation]) && m_CatOperNames.ContainsKey(aclOperation))
                                permissions.Add((string)m_CatOperNames[aclOperation]);
                        }
                        foreach (ReportOperation aclOperation in ace.ReportOperations)
                        {
                            if (!permissions.Contains((string)m_RptOperNames[aclOperation]) && m_RptOperNames.ContainsKey(aclOperation))
                                permissions.Add((string)m_RptOperNames[aclOperation]);
                        }
                        foreach (FolderOperation aclOperation in ace.FolderOperations)
                        {
                            if (!permissions.Contains((string)m_FldOperNames[aclOperation]) && m_FldOperNames.ContainsKey(aclOperation))
                                permissions.Add((string)m_FldOperNames[aclOperation]);
                        }
                        foreach (ResourceOperation aclOperation in ace.ResourceOperations)
                        {
                            if (!permissions.Contains((string)m_ResOperNames[aclOperation]) && m_ResOperNames.ContainsKey(aclOperation))
                                permissions.Add((string)m_ResOperNames[aclOperation]);
                        }
                        foreach (DatasourceOperation aclOperation in ace.DatasourceOperations)
                        {
                            if (!permissions.Contains((string)m_DSOperNames[aclOperation]) && m_DSOperNames.ContainsKey(aclOperation))
                                permissions.Add((string)m_DSOperNames[aclOperation]);
                        }
                        foreach (ModelOperation aclOperation in ace.ModelOperations)
                        {
                            if (!permissions.Contains((string)m_MdlOperNames[aclOperation]) && m_MdlOperNames.ContainsKey(aclOperation))
                                permissions.Add((string)m_MdlOperNames[aclOperation]);
                        }
                        foreach (ModelItemOperation aclOperation in ace.ModelItemOperations)
                        {
                            if (!permissions.Contains((string)m_MitOperNames[aclOperation]) && m_MitOperNames.ContainsKey(aclOperation))
                                permissions.Add((string)m_MitOperNames[aclOperation]);
                        }
                    }
                }

                //If user not found, no permissions will be set - so apply default permissions instead if they exist.
                if (permissions.Count == 0 && defaultPermissions.Count > 0)
                    permissions = defaultPermissions;
            }

            return permissions;
        }

        //Operation mappings
        private static Hashtable m_CatOperNames;
        private static Hashtable m_FldOperNames;
        private static Hashtable m_RptOperNames;
        private static Hashtable m_ResOperNames;
        private static Hashtable m_DSOperNames;
        private static Hashtable m_MdlOperNames;
        private static Hashtable m_MitOperNames;

        // Utility method used to create mappings to the various
        // operations in Reporting Services. These mappings support
        // the implementation of the GetPermissions method.
        private static void InitializeMaps()
        {
            // create operation names data
            m_CatOperNames = new Hashtable();
            m_CatOperNames.Add(CatalogOperation.CreateRoles, OperationNames.OperCreateRoles);
            m_CatOperNames.Add(CatalogOperation.DeleteRoles, OperationNames.OperDeleteRoles);
            m_CatOperNames.Add(CatalogOperation.ReadRoleProperties, OperationNames.OperReadRoleProperties);
            m_CatOperNames.Add(CatalogOperation.UpdateRoleProperties, OperationNames.OperUpdateRoleProperties);
            m_CatOperNames.Add(CatalogOperation.ReadSystemProperties, OperationNames.OperReadSystemProperties);
            m_CatOperNames.Add(CatalogOperation.UpdateSystemProperties, OperationNames.OperUpdateSystemProperties);
            m_CatOperNames.Add(CatalogOperation.GenerateEvents, OperationNames.OperGenerateEvents);
            m_CatOperNames.Add(CatalogOperation.ReadSystemSecurityPolicy, OperationNames.OperReadSystemSecurityPolicy);
            m_CatOperNames.Add(CatalogOperation.UpdateSystemSecurityPolicy, OperationNames.OperUpdateSystemSecurityPolicy);
            m_CatOperNames.Add(CatalogOperation.CreateSchedules, OperationNames.OperCreateSchedules);
            m_CatOperNames.Add(CatalogOperation.DeleteSchedules, OperationNames.OperDeleteSchedules);
            m_CatOperNames.Add(CatalogOperation.ReadSchedules, OperationNames.OperReadSchedules);
            m_CatOperNames.Add(CatalogOperation.UpdateSchedules, OperationNames.OperUpdateSchedules);
            m_CatOperNames.Add(CatalogOperation.ListJobs, OperationNames.OperListJobs);
            m_CatOperNames.Add(CatalogOperation.CancelJobs, OperationNames.OperCancelJobs);
            m_CatOperNames.Add(CatalogOperation.ExecuteReportDefinition, OperationNames.ExecuteReportDefinition);

            if (m_CatOperNames.Count != Enum.GetValues(typeof(CatalogOperation)).Length)
                throw new Exception("Number of catalog names don't match.");

            m_FldOperNames = new Hashtable();
            m_FldOperNames.Add(FolderOperation.CreateFolder, OperationNames.OperCreateFolder);
            m_FldOperNames.Add(FolderOperation.Delete, OperationNames.OperDelete);
            m_FldOperNames.Add(FolderOperation.ReadProperties, OperationNames.OperReadProperties);
            m_FldOperNames.Add(FolderOperation.UpdateProperties, OperationNames.OperUpdateProperties);
            m_FldOperNames.Add(FolderOperation.CreateReport, OperationNames.OperCreateReport);
            m_FldOperNames.Add(FolderOperation.CreateResource, OperationNames.OperCreateResource);
            m_FldOperNames.Add(FolderOperation.ReadAuthorizationPolicy, OperationNames.OperReadAuthorizationPolicy);
            m_FldOperNames.Add(FolderOperation.UpdateDeleteAuthorizationPolicy, OperationNames.OperUpdateDeleteAuthorizationPolicy);
            m_FldOperNames.Add(FolderOperation.CreateDatasource, OperationNames.OperCreateDatasource);
            m_FldOperNames.Add(FolderOperation.CreateModel, OperationNames.OperCreateModel);

            if (m_FldOperNames.Count != Enum.GetValues(typeof(FolderOperation)).Length)
                throw new Exception("Number of folder names don't match.");

            m_RptOperNames = new Hashtable();
            m_RptOperNames.Add(ReportOperation.Delete, OperationNames.OperDelete);
            m_RptOperNames.Add(ReportOperation.ReadProperties, OperationNames.OperReadProperties);
            m_RptOperNames.Add(ReportOperation.UpdateProperties, OperationNames.OperUpdateProperties);
            m_RptOperNames.Add(ReportOperation.UpdateParameters, OperationNames.OperUpdateParameters);
            m_RptOperNames.Add(ReportOperation.ReadDatasource, OperationNames.OperReadDatasources);
            m_RptOperNames.Add(ReportOperation.UpdateDatasource, OperationNames.OperUpdateDatasources);
            m_RptOperNames.Add(ReportOperation.ReadReportDefinition, OperationNames.OperReadReportDefinition);
            m_RptOperNames.Add(ReportOperation.UpdateReportDefinition, OperationNames.OperUpdateReportDefinition);
            m_RptOperNames.Add(ReportOperation.CreateSubscription, OperationNames.OperCreateSubscription);
            m_RptOperNames.Add(ReportOperation.DeleteSubscription, OperationNames.OperDeleteSubscription);
            m_RptOperNames.Add(ReportOperation.ReadSubscription, OperationNames.OperReadSubscription);
            m_RptOperNames.Add(ReportOperation.UpdateSubscription, OperationNames.OperUpdateSubscription);
            m_RptOperNames.Add(ReportOperation.CreateAnySubscription, OperationNames.OperCreateAnySubscription);
            m_RptOperNames.Add(ReportOperation.DeleteAnySubscription, OperationNames.OperDeleteAnySubscription);
            m_RptOperNames.Add(ReportOperation.ReadAnySubscription, OperationNames.OperReadAnySubscription);
            m_RptOperNames.Add(ReportOperation.UpdateAnySubscription, OperationNames.OperUpdateAnySubscription);
            m_RptOperNames.Add(ReportOperation.UpdatePolicy, OperationNames.OperUpdatePolicy);
            m_RptOperNames.Add(ReportOperation.ReadPolicy, OperationNames.OperReadPolicy);
            m_RptOperNames.Add(ReportOperation.DeleteHistory, OperationNames.OperDeleteHistory);
            m_RptOperNames.Add(ReportOperation.ListHistory, OperationNames.OperListHistory);
            m_RptOperNames.Add(ReportOperation.ExecuteAndView, OperationNames.OperExecuteAndView);
            m_RptOperNames.Add(ReportOperation.CreateResource, OperationNames.OperCreateResource);
            m_RptOperNames.Add(ReportOperation.CreateSnapshot, OperationNames.OperCreateSnapshot);
            m_RptOperNames.Add(ReportOperation.ReadAuthorizationPolicy, OperationNames.OperReadAuthorizationPolicy);
            m_RptOperNames.Add(ReportOperation.UpdateDeleteAuthorizationPolicy, OperationNames.OperUpdateDeleteAuthorizationPolicy);
            m_RptOperNames.Add(ReportOperation.Execute, OperationNames.OperExecute);
            m_RptOperNames.Add(ReportOperation.CreateLink, OperationNames.OperCreateLink);

            if (m_RptOperNames.Count != Enum.GetValues(typeof(ReportOperation)).Length)
                throw new Exception("Number of report names don't match.");

            m_ResOperNames = new Hashtable();
            m_ResOperNames.Add(ResourceOperation.Delete, OperationNames.OperDelete);
            m_ResOperNames.Add(ResourceOperation.ReadProperties, OperationNames.OperReadProperties);
            m_ResOperNames.Add(ResourceOperation.UpdateProperties, OperationNames.OperUpdateProperties);
            m_ResOperNames.Add(ResourceOperation.ReadContent, OperationNames.OperReadContent);
            m_ResOperNames.Add(ResourceOperation.UpdateContent, OperationNames.OperUpdateContent);
            m_ResOperNames.Add(ResourceOperation.ReadAuthorizationPolicy, OperationNames.OperReadAuthorizationPolicy);
            m_ResOperNames.Add(ResourceOperation.UpdateDeleteAuthorizationPolicy, OperationNames.OperUpdateDeleteAuthorizationPolicy);

            if (m_ResOperNames.Count != Enum.GetValues(typeof(ResourceOperation)).Length)
                throw new Exception("Number of resource names don't match.");

            m_DSOperNames = new Hashtable();
            m_DSOperNames.Add(DatasourceOperation.Delete, OperationNames.OperDelete);
            m_DSOperNames.Add(DatasourceOperation.ReadProperties, OperationNames.OperReadProperties);
            m_DSOperNames.Add(DatasourceOperation.UpdateProperties, OperationNames.OperUpdateProperties);
            m_DSOperNames.Add(DatasourceOperation.ReadContent, OperationNames.OperReadContent);
            m_DSOperNames.Add(DatasourceOperation.UpdateContent, OperationNames.OperUpdateContent);
            m_DSOperNames.Add(DatasourceOperation.ReadAuthorizationPolicy, OperationNames.OperReadAuthorizationPolicy);
            m_DSOperNames.Add(DatasourceOperation.UpdateDeleteAuthorizationPolicy, OperationNames.OperUpdateDeleteAuthorizationPolicy);

            if (m_DSOperNames.Count != Enum.GetValues(typeof(DatasourceOperation)).Length)
                throw new Exception("Number of datasource names don't match.");

            m_MdlOperNames = new Hashtable();
            m_MdlOperNames.Add(ModelOperation.Delete, OperationNames.OperDelete);
            m_MdlOperNames.Add(ModelOperation.ReadProperties, OperationNames.OperReadProperties);
            m_MdlOperNames.Add(ModelOperation.UpdateProperties, OperationNames.OperUpdateProperties);
            m_MdlOperNames.Add(ModelOperation.ReadDatasource, OperationNames.OperReadDatasources);
            m_MdlOperNames.Add(ModelOperation.UpdateDatasource, OperationNames.OperUpdateDatasources);
            m_MdlOperNames.Add(ModelOperation.ReadContent, OperationNames.OperReadContent);
            m_MdlOperNames.Add(ModelOperation.UpdateContent, OperationNames.OperUpdateContent);
            m_MdlOperNames.Add(ModelOperation.ReadAuthorizationPolicy, OperationNames.OperReadAuthorizationPolicy);
            m_MdlOperNames.Add(ModelOperation.UpdateDeleteAuthorizationPolicy, OperationNames.OperUpdateDeleteAuthorizationPolicy);
            m_MdlOperNames.Add(ModelOperation.ReadModelItemAuthorizationPolicies, OperationNames.OperReadModelItemSecurityPolicies);
            m_MdlOperNames.Add(ModelOperation.UpdateModelItemAuthorizationPolicies, OperationNames.OperUpdateModelItemSecurityPolicies);

            if (m_MdlOperNames.Count != Enum.GetValues(typeof(ModelOperation)).Length)
                throw new Exception("Number of model names don't match.");

            m_MitOperNames = new Hashtable();
            m_MitOperNames.Add(ModelItemOperation.ReadProperties, OperationNames.OperReadProperties);

            if (m_MitOperNames.Count != Enum.GetValues(typeof(ModelItemOperation)).Length)
                throw new Exception("Number of model item names don't match.");

        }

        /// <summary>
        /// You must implement SetConfiguration as required by IExtension
        /// </summary>
        /// <param name="configuration">Configuration data as an XML
        /// string that is stored along with the Extension element in
        /// the configuration file.</param>
        public void SetConfiguration(string configuration)
        {
            // Retrieve admin user name from the config settings
            // and verify
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(configuration);
            if (doc.DocumentElement.Name == "AdminConfiguration")
            {
                foreach (XmlNode child in doc.DocumentElement.ChildNodes)
                {
                    if (child.Name == "UserName")
                        m_adminUserName = child.InnerText;
                    else
                        throw new Exception("Unrecognized configuration element.");
                }
            }
            else
                throw new Exception("Bryan error loading config data.");


            // Retrive default identity from the web config settings
            m_defaultIdentiy = AuthenticationUtilities.GetDefaultIdentity();
        }

        public string LocalizedName
        {
            get
            {
                //Return a localized name for this extension if required.
                return null;
            }
        }
    }
}
