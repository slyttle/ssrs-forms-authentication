using System;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.Configuration;

namespace Microsoft.Samples.ReportingServices.CustomSecurity
{
    public class Login : System.Web.UI.Page
    {
        protected Label LblUser;
        protected TextBox TxtPwd;
        protected TextBox TxtUser;
        protected Button BtnRegister;
        protected Button BtnLogon;
        protected Label lblMessage;
        protected Label Label1;
        protected Label LblPwd;

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["authToken"]))
            {
                string Username = AuthenticationUtilities.VerifyAuthToken(Request.QueryString["authToken"]);

                //If authentication token is valid - redirect to Reports. Otherwise redirect to landing page.
                if (!string.IsNullOrEmpty(Username))
                    FormsAuthentication.RedirectFromLoginPage(Username, false);
                else
                    Response.Redirect("/?message=Invalid authorisation token, or session has expired.", false);
            }

            //Debug mode is not enabled - return to landing page. Otherwise display this page.
            else if (String.IsNullOrEmpty(WebConfigurationManager.AppSettings["DebugMode"]))
                Response.Redirect("/", false);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            BtnLogon.Click += new EventHandler(ServerBtnLogon_Click);
            BtnRegister.Click += new EventHandler(BtnRegister_Click);
            Load += new EventHandler(Page_Load);

        }
        #endregion

        private void BtnRegister_Click(object sender, System.EventArgs e)
        {
            if (AuthenticationUtilities.ValidateUserName(TxtUser.Text))
            {
                try
                {
                    AuthenticationUtilities.StoreAccountDetails(TxtUser.Text, TxtPwd.Text);
                }
                catch (Exception ex)
                {
                    lblMessage.Text = ex.Message;
                }
            }
            else
            {
                lblMessage.Text = "The user name that was entered contains characters " +
                   "not supported by report server custom authentication.";
            }
        }

        private void ServerBtnLogon_Click(object sender, System.EventArgs e)
        {
            try
            {
                lblMessage.Text = "Login successful: User is authenticated.";
                if (AuthenticationUtilities.VerifyPassword(TxtUser.Text, TxtPwd.Text))
                    FormsAuthentication.RedirectFromLoginPage(TxtUser.Text, false);
                else
                    lblMessage.Text = "Invalid username or password.";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                return;
            }
        }
    }
}
