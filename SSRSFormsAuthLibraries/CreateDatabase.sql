USE [master]
GO

IF EXISTS (SELECT NULL FROM master..sysdatabases WHERE name = 'RSAuthentication')
    DROP DATABASE [RSAuthentication]
GO

CREATE DATABASE [RSAuthentication]
GO

USE [RSAuthentication]
GO

--Create Applications Table
CREATE TABLE [dbo].[Application](
	[kApplication] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationID] [uniqueidentifier] NOT NULL,
	[ApplicationSecret] [varchar](128) NOT NULL,
	[Description] [nvarchar](1000) NULL,
    CONSTRAINT [PK_Application] PRIMARY KEY CLUSTERED (
	    [kApplication] ASC
    ) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_ApplicationID] ON [dbo].[Application] (
	[ApplicationID] ASC
)
GO


--Create Users Table
CREATE TABLE [dbo].[Users](
	[kApplication] [int] NOT NULL,
	[Username] [nvarchar](260) NOT NULL,
	[Password] [varchar](40) NULL,
	[Token] [varchar](128) NULL,
	[Expires] [datetime] NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED (
	    [kApplication] ASC,
	    [Username] ASC
    ) ON [PRIMARY]
) ON [PRIMARY]
GO


-- =============================================
-- Author:		Sean Lyttle
-- Create date: 25/03/2016
-- Description:	Fetch application details with a given ApplicationID and Application Secret.
-- =============================================
CREATE PROCEDURE [dbo].[GetApplication]
    @AppID uniqueidentifier,
    @AppSecret varchar(128) = NULL
AS

SELECT
    ApplicationID,
    Description
FROM
    Application
WHERE
    ApplicationID = @AppID
    AND (
        @AppSecret IS NULL OR ApplicationSecret = @AppSecret COLLATE SQL_Latin1_General_CP1_CS_AS --Case sensitive secret
    );
GO

-- =============================================
-- Author:		Sean Lyttle
-- Create date: 23/03/2016
-- Description:	Fetch user details from a given Username OR Authentication Token.
-- =============================================
CREATE PROCEDURE [dbo].[GetUser]
    @AppID uniqueidentifier = NULL,
    @Username nvarchar(260) = NULL,
    @Password varchar(40) = NULL,
    @Token varchar(128) = NULL
AS

SELECT
    Username
FROM
    Users u
    INNER JOIN Application a ON a.kApplication = u.kApplication
WHERE
    ApplicationID = @AppID
    AND (
        (Username = @Username AND (@Password IS NULL OR Password = @Password COLLATE SQL_Latin1_General_CP1_CS_AS)) --Case sensitive password
        OR
        (Token = @Token COLLATE SQL_Latin1_General_CP1_CS_AS AND CURRENT_TIMESTAMP <= ISNULL(Expires, CURRENT_TIMESTAMP))
    );

--Expire token immediately so it can only be used once.
IF @Token IS NOT NULL
BEGIN
    UPDATE
        Users
    SET
        Expires = CURRENT_TIMESTAMP
    WHERE
        Token = @Token COLLATE SQL_Latin1_General_CP1_CS_AS
        AND CURRENT_TIMESTAMP < Expires;
END
GO

-- =============================================
-- Author:		Sean Lyttle
-- Create date: 23/03/2016
-- Description:	Records a new Authentication Token for the given user and refreshes the expiary datetime.
-- =============================================
CREATE PROCEDURE [dbo].[RefreshAuthToken]
    @AppID uniqueidentifier,
    @Username nvarchar(260),
    @Token varchar(128)
AS

--Fetch kApplication
DECLARE @kApplication int = (SELECT kApplication FROM Application WHERE ApplicationID = @AppID);

--If Application exists
IF(@kApplication IS NOT NULL)
BEGIN
    --If user has logged in before, update their auth token.
    IF EXISTS (SELECT NULL FROM Users WHERE Username = @Username AND kApplication = @kApplication)
    BEGIN
        UPDATE
            Users
        SET
            Token = @Token,
            Expires = DATEADD(MI, 5, CURRENT_TIMESTAMP)--Expires in 5 Minutes
        WHERE
            Username = @Username
            AND kApplication = @kApplication;
    END
    ELSE --Otherwise create a new auth token.
    BEGIN
        INSERT INTO
            Users
        VALUES (
            @kApplication,
            @Username,
            NULL, --Password
            @Token,
            DATEADD(MI, 5, CURRENT_TIMESTAMP)--Expires in 5 Minutes
        );
    END
END
GO

-- =============================================
-- Author:		Sean Lyttle
-- Create date: 23/03/2016
-- Description:	Records a new Password for the given user.
-- =============================================
CREATE PROCEDURE [dbo].[RefreshPassword]
    @AppID uniqueidentifier,
    @Username nvarchar(260),
    @Password varchar(40)
AS

--Fetch kApplication
DECLARE @kApplication int = (SELECT kApplication FROM Application WHERE ApplicationID = @AppID);

--If Application exists
IF(@kApplication IS NOT NULL)
BEGIN
    --If user has logged in before, update their auth token.
    IF EXISTS (SELECT NULL FROM Users WHERE Username = @Username AND kApplication = @kApplication)
    BEGIN
        UPDATE
            Users
        SET
            Password = @Password
        WHERE
            Username = @Username
            AND kApplication = @kApplication;
    END
    ELSE --Otherwise create a new auth token.
    BEGIN
        INSERT INTO
            Users
        VALUES (
            @kApplication,
            @Username,
            @Password,
            NULL, --Token
            NULL  --Expires
        );
    END
END
GO

--Insert an application record.
INSERT INTO [dbo].[Application] (
    [ApplicationID],
    [ApplicationSecret],
    [Description])
VALUES (
    '0F6E93E6-D96C-454A-A9F1-6646AE139F59',
    '4vUfWtARkW9uaMquwZasGvP2pgIYGUbZrN8AKrBNaShihZ7W79dntWEYzH8dcg9hHDJUvjrvGLMRUx2vGU9gI5UGnQLQ02SguVmRoQsnxm9if5XRD9sYfGxnkigygEtF',
    'Single Sign On application for the eDofE portal.'
);
GO


-- Add a login for the SSRS account
exec sp_grantlogin [NT SERVICE\ReportServer];
-- Add a database login for the SSRS account
exec sp_grantdbaccess [NT SERVICE\ReportServer];
-- Grant execute permissions to the stored procedures
grant execute on [dbo].[GetUser] to [NT SERVICE\ReportServer];
grant execute on [dbo].[RefreshAuthToken] to [NT SERVICE\ReportServer];
grant execute on [dbo].[RefreshPassword] to [NT SERVICE\ReportServer];


