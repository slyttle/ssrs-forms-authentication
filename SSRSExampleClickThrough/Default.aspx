﻿<%@ Page Language="C#" %>

<!DOCTYPE html>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
            Username.Text = Request.QueryString["username"];
    }
    protected void ReportsButton_Click(object sender, EventArgs e)
    {
        string SingleSignOnUrl = URL.Text + "/SSO.aspx";

        if (!string.IsNullOrEmpty(Username.Text))
        {
            using (System.Net.WebClient webRequest = new System.Net.WebClient())
            {
                NameValueCollection postData = new NameValueCollection();
                postData["AppID"] = "0f6e93e6-d96c-454a-a9f1-6646ae139f59";
                postData["AppSecret"] = "4vUfWtARkW9uaMquwZasGvP2pgIYGUbZrN8AKrBNaShihZ7W79dntWEYzH8dcg9hHDJUvjrvGLMRUx2vGU9gI5UGnQLQ02SguVmRoQsnxm9if5XRD9sYfGxnkigygEtF";
                postData["Username"] = Username.Text;

                byte[] webResponse = webRequest.UploadValues(SingleSignOnUrl, postData);

                string AuthToken = Encoding.Default.GetString(webResponse);

                if (AuthToken.StartsWith("Error"))
                    Response.Write(AuthToken);
                else
                    Response.Redirect(string.Concat(SingleSignOnUrl, "?AuthToken=", Uri.EscapeDataString(AuthToken)));
            }
        }

    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Please enter report manager url: <asp:TextBox runat="server" ID="URL" Text="https://reporting-uat.edofe.org/Reports_DOFEUAT" Width="400"></asp:TextBox>
        <br />
        Please enter your eDofE username: <asp:TextBox runat="server" ID="Username"></asp:TextBox>
        <br />
        <asp:Button runat="server" ID="ReportsButton" Text="Open Reports" OnClick="ReportsButton_Click" />
    </div>
    </form>
</body>
</html>
